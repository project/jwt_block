<?php

namespace Drupal\jwt_block\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A256CBCHS512;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP256;
use Jose\Component\Encryption\JWEBuilder;
use Jose\Component\Encryption\Serializer\CompactSerializer;
use Jose\Component\KeyManagement\JWKFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'JWT' block.
 *
 * @Block(
 *  id = "jwt_block",
 *  admin_label = @Translation("JWT block"),
 * )
 */
class JWTBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * JWTBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The state service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger->get('jwt_block');
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory */
    $logger_factory = $container->get('logger.factory');
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $container->get('config.factory');
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $logger_factory,
      $config_factory
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $build = [
      '#type' => 'form',
      '#form_id' => 'jwt_block_form',
      '#action' => $config['endpoint'],
    ];

    $key = $this->configFactory->get('key.key.' . $config['jwk']);
    $key_provider = $key->get('key_provider');
    if (!$key_provider) {
      return ['#markup' => $this->t('Invalid key.')];
    }

    if ($key_provider == 'file') {
      $private_key_file = $key->get('key_provider_settings.file_location');
      try {
        $jwk = JWKFactory::createFromKeyFile($private_key_file);
      } catch (\Exception $e) {
        $this->logger->error($e->getMessage());
        return ['#markup' => $e->getMessage()];
      }
    } else {
      $jwk = JWKFactory::createFromKey($key->get('key_provider_settings.key_value'));
    }

    $payload = $this->getPayload();

    try {
      $token = $this->getEncryptedToken($jwk, $payload);
    } catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      return ['#markup' => $e->getMessage()];
    }
    $build['token'] = [
      '#type' => 'hidden',
      '#name' => 'token',
      '#value' => $token,
    ];

    $build['submit'] = [
      '#type' => 'submit',
      '#value' => $config['link_text'],
    ];

    // Do not cache this.
    $build['#cache']['max-age'] = 0;

    return $build;
  }

  /**
   * Get the payload.
   *
   * @return array
   */
  protected function getPayload() {
    $timestamp = time();
    return [
      'iat' => $timestamp,
      'exp' => $timestamp + 3600,
      'value' => 'Test value'
    ];
  }

  /**
   * Get the encrypted token.
   *
   * @param $jwk
   *   The JSON Web Key.
   * @param $payload
   *   The payload.
   *
   * @return mixed
   * @throws \Exception
   */
  protected function getEncryptedToken($jwk, $payload) {
    $keyEncryptionAlgorithmManager = new AlgorithmManager([
      new RSAOAEP256(),
    ]);

    // The content encryption algorithm manager with the A256CBC-HS256 algorithm.
    $contentEncryptionAlgorithmManager = new AlgorithmManager([
      new A256CBCHS512(),
    ]);

    $jweBuilder = new JWEBuilder(
      $keyEncryptionAlgorithmManager,
      $contentEncryptionAlgorithmManager,
    );

    if (is_array($payload)) {
      $payload = Json::encode($payload);
    }
    $jwe = $jweBuilder->create()
      ->withPayload($payload)
      ->withSharedProtectedHeader([
        'alg' => 'RSA-OAEP-256',
        'enc' => 'A256CBC-HS512'
      ])
      ->addRecipient($jwk)
      ->build();

    $serializer = new CompactSerializer();

    return $serializer->serialize($jwe, 0);
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#default_value' => $config['link_text'] ?? '',
      '#description' => $this->t('Enter the text that will be visible on the link.'),
      '#required' => TRUE,
    ];

    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#default_value' => $config['endpoint'] ?? '',
      '#description' => $this->t('Enter the endpoint.'),
      '#required' => TRUE,
    ];

    $form['jwt_algorithm'] = [
      '#type' => 'select',
      '#title' => $this->t('Algorithm'),
      '#options' => [
        'RS256' => 'RSASSA-PKCS1-v1_5 using SHA-256 (RS256)',
      ],
      '#ajax' => [
        'callback' => '::ajaxKeysCallback',
        'event' => 'change',
        'wrapper' => 'jwt-key-container',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
      '#default_value' => $config['jwt_algorithm'] ?? 'RS256',
      '#disabled' => TRUE,
    ];

    $form['key_container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="jwt-key-container">',
      '#suffix' => '</div>',
    ];

    $form['key_container']['jwk'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Private key'),
      '#default_value' => $config['jwk'] ?? NULL,
      '#key_filters' => [
        'type' => 'jwt_rs',
      ],
      '#validated' => TRUE,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * AJAX Function callback.
   *
   * @param array $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal formstate object.
   *
   * @return mixed
   *   Updated AJAXed form.
   */
  public function ajaxKeysCallback(array &$form, FormStateInterface $form_state) {
    return $form['key_container'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['link_text'] = $form_state->getValue('link_text');
    $this->configuration['endpoint'] = $form_state->getValue('endpoint');
    $this->configuration['jwt_algorithm'] = $form_state->getValue('jwt_algorithm');
    $this->configuration['jwk'] = $form_state->getValue(['key_container', 'jwk']);
  }

}
