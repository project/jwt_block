# JWT Block

> This module contains a block with a link to an endpoint using a JWT Token, extending this block you can change the payload of the token.
Works with Key module and JSON Web Token Authentication (JWT) modules.


## Dependencies

### Modules:

* Key module
* JSON Web Token Authentication (JWT)

### PHP Extensions:

Requires the GMP PHP extension.
