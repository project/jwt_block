<?php

namespace Drupal\Tests\jwt_block\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests JWT block.
 *
 * @group jwt_block
 */
class JWTBlockTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['block', 'key', 'jwt', 'jwt_block', 'jwt_token_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to create and edit books and to administer blocks.
   *
   * @var object
   */
  protected $adminUser;

  /**
   * An authenticated user to test JWT block.
   *
   * @var object
   */
  protected $normalUser;

  protected function setUp(): void {
    parent::setUp();

    // Create an admin user, log in and enable test blocks.
    $this->adminUser = $this->drupalCreateUser([
      'administer blocks',
      'access administration pages',
    ]);
    $this->drupalLogin($this->adminUser);

    // Create normal user.
    $this->normalUser = $this->drupalCreateUser();
  }

  /**
   * Test JWE block.
   */
  public function testJWEBlock() {
    $this->drupalLogin($this->normalUser);
    $this->drupalGet('');
    $web_assert = $this->assertSession();
    $web_assert->pageTextContains('JWE block');

    $this->submitForm([], t('Login to endpoint'));

    $web_assert = $this->assertSession();
    $web_assert->pageTextContains('Logged in with payload');
  }

}
