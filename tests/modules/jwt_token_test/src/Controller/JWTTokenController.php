<?php

namespace Drupal\jwt_token_test\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Encryption\Algorithm\ContentEncryption\A256CBCHS512;
use Jose\Component\Encryption\Algorithm\KeyEncryption\RSAOAEP256;
use Jose\Component\Encryption\JWEDecrypter;
use Jose\Component\Encryption\JWELoader;
use Jose\Component\Encryption\Serializer\CompactSerializer;
use Jose\Component\Encryption\Serializer\JWESerializerManager;
use Jose\Component\KeyManagement\JWKFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JWTTokenController.
 *
 * @package Drupal\jwt_token_test\Controller
 */
class JWTTokenController extends ControllerBase {

  /**
   * Test endpoint.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array
   */
  public function testJWEEndpoint(Request $request) {
    $token = $request->request->get('token');
    if (empty($token)) {
      return ['#markup' => 'Error: Empty token'];
    }

    $key = \Drupal::configFactory()->get('key.key.test_key');
    $jwk = JWKFactory::createFromKey($key->get('key_provider_settings.key_value'));
    if (!$jwk) {
      return ['#markup' => 'Error: Could not load the key'];
    }

    $keyEncryptionAlgorithmManager = new AlgorithmManager([new RSAOAEP256()]);
    $contentEncryptionAlgorithmManager = new AlgorithmManager([new A256CBCHS512()]);

    $jweDecrypter = new JWEDecrypter(
      $keyEncryptionAlgorithmManager,
      $contentEncryptionAlgorithmManager,
    );

    $serializerManager = new JWESerializerManager([
      new CompactSerializer(),
    ]);

    $jweLoader = new JWELoader(
      $serializerManager,
      $jweDecrypter,
      NULL
    );

    $jwt = $jweLoader->loadAndDecryptWithKey($token, $jwk, 0);

    return [
      '#markup' => $this->t('Logged in with payload: ') . Json::encode($jwt->getPayload()),
    ];
  }

}
